//HW 02
//Yingjun Huang
// 20180202
// CSE 002
/* -----------------------------------------------------------------------------------
Please calculate the following costs:
Total cost of each kind of item (i.e. total cost of pants, etc)
Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
Total cost of purchases (before tax)
Total sales tax
Total paid for this transaction, including sales tax. 
---------------------------------------------------------------------------------------
*/ 

// To display only two digit after decimal
import java.text.DecimalFormat;


class Arithmetic {
    // To display only two digit after decimal
    //private static DecimalFormat df2 = new DecimalFormat(".00");

    public static void main(String args[]) {

    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    


    //-------------------------------------------------

    // the cost of all pants 
    // # of pants * $ of pants 
    double TotalPantsCost = numPants * pantsPrice; 
    //System.out.println("Total cost of Pants is $" + df2.format(TotalPantsCost));
    System.out.println("Total cost of Pants is $" + (int)(TotalPantsCost*100)/100.0);
    // tax charged on pants
    // cost of all pants * % of sale tax
    double TaxOnPants = TotalPantsCost * paSalesTax; 
    //System.out.println("Total sale tax charged on Pants is $" + df2.format(TaxOnPants));
    System.out.println("Total sale tax charged on Pants is $" + (int)(TaxOnPants*100)/100.0);

    // the cost of all sweatshirts
    // # of sweatshirts * $ of sweatshirts 
    double TotalShirtsCost = numShirts * shirtPrice; 
    //System.out.println("Total cost of Shirts is $" + df2.format(TotalShirtsCost));
    System.out.println("Total cost of Shirts is $" + (int)(TotalShirtsCost*100)/100.0);
    // Tax charged on Shirts
    // cost of all shirts * % of sale tax
    double TaxOnShirts = TotalShirtsCost * paSalesTax; 
    //System.out.println("Total sale tax charged on Shirts is $" + df2.format(TaxOnShirts)); 
    System.out.println("Total sale tax charged on Shirts is $" + (int)(TaxOnShirts*100)/100.0);

    // the cost of all belts 
    // # of belts * $ of belts 
    double TotalBeltsCost = numBelts * beltCost; 
    //System.out.println("Total cost of Belts is $" + df2.format(TotalBeltsCost));
    System.out.println("Total cost of Belts is $" + (int)(TotalBeltsCost*100)/100.0);
    // tax charged on belts 
    // cost of all belts * % of sale tax
    double TaxOnBelts = TotalBeltsCost * paSalesTax; 
    //System.out.println("Total sale tax charged on Belts is $" + df2.format(TaxOnBelts));
    System.out.println("Total sale tax charged on Belts is $" + (int)(TaxOnBelts*100)/100.0);

    // Total cost before tax
    //sum of all cost pre tax
    double TotalBeforeTax = TotalPantsCost + TotalShirtsCost + TotalBeltsCost; 
    //System.out.println("Total cost before tax for this purchase is $" + df2.format(TotalBeforeTax));
    System.out.println("Total cost before tax for this purchase is $" + (int)(TotalBeforeTax*100)/100.0);

    //Total sale tax
    //sum of all sale tax
    double TotalSaleTax = TaxOnPants + TaxOnShirts + TaxOnBelts; 
    //System.out.println("Total Sale Tax is $" + df2.format(TotalSaleTax));
    System.out.println("Total Sale Tax is $" + (int)(TotalSaleTax*100)/100.0);

    // Total spent for this shopping trip
    //including sale tax paid to the State of PA, sum of total tax + sum of total cost for the items 
    double TotalSpent = TotalBeforeTax + TotalSaleTax; 
    //System.out.println("Total paid including sales tax is $" + df2.format(TotalSpent));
    System.out.println("Total paid including sales tax is $" + (int)(TotalSpent*100)/100.0);





    }
}