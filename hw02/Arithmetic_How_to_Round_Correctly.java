//Why not round correctly!? 

//HW 02
//Yingjun Huang
// 20180202
// CSE 002
/* -----------------------------------------------------------------------------------
Please calculate the following costs:
Total cost of each kind of item (i.e. total cost of pants, etc)
Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
Total cost of purchases (before tax)
Total sales tax
Total paid for this transaction, including sales tax. 
---------------------------------------------------------------------------------------
*/ 

// To display only two digit after decimal
import java.text.DecimalFormat;


class Arithmetic_How_to_Round_Correctly {
    // To display only two digit after decimal
    private static DecimalFormat df2 = new DecimalFormat(".00");

    public static void main(String args[]) {

    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    


    //-------------------------------------------------

    // the cost of all pants 
    double TotalPantsCost = numPants * pantsPrice; 
    System.out.println("Total cost of Pants is $" + df2.format(TotalPantsCost));
    // tax charged on pants
    double TaxOnPants = TotalPantsCost * paSalesTax; 
    System.out.println("Total sale tax charged on Pants is $" + df2.format(TaxOnPants));

    // the cost of all sweatshirts
    double TotalShirtsCost = numShirts * shirtPrice; 
    System.out.println("Total cost of Shirts is $" + df2.format(TotalShirtsCost));
    // Tax charged on Shirts
    double TaxOnShirts = TotalShirtsCost * paSalesTax; 
    System.out.println("Total sale tax charged on Shirts is $" + df2.format(TaxOnShirts)); 

    // the cost of all belts 
    double TotalBeltsCost = numBelts * beltCost; 
    System.out.println("Total cost of Belts is $" + df2.format(TotalBeltsCost));
    // tax charged on belts 
    double TaxOnBelts = TotalBeltsCost * paSalesTax; 
    System.out.println("Total sale tax charged on Belts is $" + df2.format(TaxOnBelts));

    
    // Total cost before tax 
    double TotalBeforeTax = TotalPantsCost + TotalShirtsCost + TotalBeltsCost; 
    System.out.println("Total cost before tax for this purchase is $" + df2.format(TotalBeforeTax));

    
    //Total sale tax
    double TotalSaleTax = TaxOnPants + TaxOnShirts + TaxOnBelts; 
    System.out.println("Total Sale Tax is $" + df2.format(TotalSaleTax));

    
    // Total spent for this shopping trip, including sale tax paid to the State of PA
    double TotalSpent = TotalBeforeTax + TotalSaleTax; 
    System.out.println("Total paid including sales tax is $" + df2.format(TotalSpent));





    }
}