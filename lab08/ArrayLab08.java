//  https://docs.google.com/document/d/1EB6yB3Z0eSn6lFyFM1eU0-HDzQ_Yl72r53KrOazTxNU/edit
import java.util.Arrays;

public class ArrayLab08 {
    public static void main(String[] args) {
        // Random array length
        int ALength = Random50_100(1); 
        int theArray [] = new int [ALength]; 
        // Fill in the array 
        // Print the intial array
        System.out.println("-----------------------------------");
        System.out.println("The following is the intial array: ");
        for (int i = 0; i < ALength; i++){
            theArray[i] = Random0_99(i);
            System.out.println(theArray[i]);
        }
        Arrays.sort(theArray); 
        // array range
        int ARange = GetRange(theArray);

        // Mean 
        double Mean = GetMean(theArray); 

        // SD 
        double SD = getStdDev(theArray); 

        System.out.println("------------------");
        System.out.println("The size of the array is: " + ALength);
        System.out.println("The range of the array is : " + ARange);
        System.out.println("The Mean of the array is : " + Mean);
        System.out.println("The SD of the array is : " + SD);



        System.out.println("-----------------------------------");
        System.out.println("The following is the shuffled array: ");
        shuffle(theArray);


    }

    
    public static int GetRange (int [] arr){
        // 
        Arrays.sort(arr); 
        int GetRange = arr[arr.length-1] - arr[0]; 
        return GetRange; 
    }

    public static double GetMean (int [] arr){
        // Total
        double sum = 0;
        for (int i = 0; i < arr.length; i++ ){
            sum = arr[i] + sum; 
        }
        // Average
        return sum/(arr.length); 
    }

    public static double getStdDev (int [] arr){
        double sum = 0.0, standardDeviation = 0.0;
        int length = arr.length;

        for(double num : arr) {
            sum += num;
        }

        double mean = sum/length;

        for(double num: arr) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/length);
    }
    
    public static void shuffle (int [] intArray) {
	
 
        for (int i=0; i<intArray.length; i++) {
            //find a random member to swap with
            int target = (int) (intArray.length * Math.random() );
        
        
            //swap the values
            int temp = intArray[target];
            intArray[target] = intArray[i];
            intArray[i] = temp;
        }

        for (int i = 0; i < intArray.length; i++){
            System.out.println(intArray[i]);

        }

 
    }

    public static int Random50_100 (int a){
        int Random50_100 = (int)(Math.random() * 50 + 50);
        return Random50_100; 
    }

    public static int Random0_99 (int AValue){
        int Random0_99 = (int)(Math.random() * 99);
        return Random0_99;
    }
}