// Lab 02
// 20180201
// Cyclometer
//
public class Cyclometer {
    // main method required for every Java program
   public static void main(String[] args) {

    // Input Data
    int secsTrip1 = 480;  // s of Trip 1
    int secsTrip2 = 3220;  // s of Trip 2
    int countsTrip1 = 1561;  // number of counts for trip 1 (rotations)
    int countsTrip2 = 9037; // number of counts for trip 2 (rotations)
    
    // our intermediate variables and output data. Document!
    double wheelDiameter=27.0,  // D of Wheel
    PI=3.14159, //pi
    feetPerMile=5280,  // feet in a mile
    inchesPerFoot=12,   //  inches in a foot
    secondsPerMinute=60;  //    seconds in a min

    double distanceTrip1, distanceTrip2, totalDistance;  //declearing distances values
   
    // Print trip data
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1+ " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2+ " counts.");

    // Calculate distances
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    // Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;

    // Print computed distances
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");


}  //end of main method   
} //end of class