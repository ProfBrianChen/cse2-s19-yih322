import java.util.Arrays;

public class SELECTION_SORT_Lab11{
    public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        int iterBest = selectionSort(myArrayBest);
        System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		int iterWorst = selectionSort(myArrayWorst);
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
	}


    	/** The method for sorting the numbers */
	public static int selectionSort(int[] arr) { 
		// Prints the initial array (you must insert another
		// print out statement later in the code to show the 
        // array as it’s being sorted)
		System.out.println(Arrays.toString(arr));
		// Initialize counter for iterations
		int iterations = 0;

		for (int i = 0; i < arr.length - 1; i++) {
			// Update the iterations counter
			iterations++;
			
            int Index = i;  
            for (int j = i + 1; j < arr.length; j++){  // -1 for preveting out of bound situation 
                if (arr[j] < arr[Index]){    // if the current index is lower...
                    Index = j;               // the lower value is at this index
                    iterations++;
                
                }                                   // lower value found at the lower index 
            }  

            if (Index != i) { 
                // COMPLETE THE CODE TO MAKE THE SWAP 
                int smallerNumber = arr[Index];  // the value at the index which is lower
                iterations++;
                arr[Index] = arr[i];             // the index of the lower value == value of the current value 
                iterations++;
                arr[i] = smallerNumber;          // the new current index is == value of the found lower index 
                iterations++;
                System.out.println(Arrays.toString(arr));
                iterations++;
			}

        }
        


		return iterations;
	}

}