// https://docs.google.com/document/d/1i1irJTtcjk05Ru3p0_bGNDo0saOrFP_smr-cubs-W80/edit#heading=h.7busmylpdufh
import java.util.Arrays;
public class InsertionSortLab11 {
	public static void main(String[] args) {
		int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
        int iterBest = insertionSort(myArrayBest);
        System.out.println("The total number of operations performed on the sorted array: " + iterBest);
		int iterWorst = insertionSort(myArrayWorst);

		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst);
    }
    	/** The method for sorting the numbers */
	public static int insertionSort(int[] array) {
		// Prints the initial array (you must insert another
		// print out statement later in the code)
		System.out.println(Arrays.toString(array));
		//Initialize counter for iterations
        int iterations = 0;
        
        int n = array.length;  
        for (int j = 1; j < n; j++) {  
            int key = array[j];  
            int i = j-1;  
            while ( (i > -1) && ( array [i] > key ) ) {  
                array [i+1] = array [i];  

                iterations++;
                i--;  
            }  
            array[i+1] = key;
            System.out.println(Arrays.toString(array));
            iterations++;
  
        }  
		return iterations;
	}
}
