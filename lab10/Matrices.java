// CSE 002 Lab 10 
// https://docs.google.com/document/d/18jv3iBCgxZkt5knnR8KhAaEPmcP6FDyrfYdDKTZdHDQ/edit#heading=h.s0u181qt15gv
// Imports scanners
import java.util.Scanner;
import java.util.Arrays;

public class Matrices {
    public static void main(String[] args) {
        
        // Declare Values for Width, Height, and format for two arrays 
        int widthA = 0; int heightA = 0; boolean formatA = true; // True for row-, false of column | A
        int widthB = 0; int heightB = 0; boolean formatB = false; // True for row-, false of column | B
        widthA = RanNum2_10(); heightA = RanNum2_10(); 
        widthB = widthA; heightB = heightA; 
        System.out.println("A & B Width is: " + widthA + " and height is: " + heightA);
        
        //Array A and B
        int A [][] = new int [widthA][heightA];
        A = increasingMatrix(widthA, heightA, formatA);
        int B [][] = new int [widthB][heightB];
        B = increasingMatrix(widthB, heightB, formatB);

        System.out.println("-----------A: ");
        printMatrix(A, formatA); 
        System.out.println("-----------B: ");
        printMatrix(B, formatB); 

        int widthC = 0; int heightC = 0; boolean formatC = true; // True for row-, false of column | C
        widthC = RanNum2_10(); heightC = RanNum2_10(); 
        int C [][] = new int [widthC][heightC];
        C = increasingMatrix(widthC, heightC, formatC);
        System.out.println("C Width is: " + widthC + " and height is: " + heightC);
        System.out.println("-----------C: ");
        printMatrix(C, formatC); 


        int A_B [][] = addMatrix(A, formatA, B, formatB); 
        System.out.println("Added A + B: ");
        printMatrix(A_B, formatA); 

        int A_C [][] = addMatrix(A, formatA, C, formatC); 
        System.out.println("Added A + C: ");
        printMatrix(A_C, formatA); 

    }

    public static int [][] increasingMatrix(int width, int height, boolean format){
        int RowincreasingMatrix [][] = new int [height][width];
        int ColincreasingMatrix [][] = new int [width][height];

        int i = 1; 
        if (format == true){ // Row-major
            while (i<= width*height){
                for (int c = 0; c< height; c++){
                    for (int r = 0; r< width; r++){
                        RowincreasingMatrix [c][r] = i; 
                        i++;
                    }
                }
            }
            //System.out.println(Arrays.deepToString(RowincreasingMatrix));
            return RowincreasingMatrix; 

        } else if (format == false) {    // Column-major
            while (i<= width*height){

                for (int c = 0; c < height; c++){
                    for (int r = 0; r < width; r++){
                        ColincreasingMatrix [r][c] = i; 
                        i++;
                    }
                }
    
            }
            //System.out.println(Arrays.deepToString(ColincreasingMatrix));
            return ColincreasingMatrix; 
        }
        return RowincreasingMatrix; 
    }

    public static void printMatrix( int[][] arr, boolean format ){
        int i = 0;
        if (arr == null){
            System.out.println();
            i =2; 
        }

        while (i < 1){
        // row 
        if (format == true){
            for(int column =0; column <  arr.length; column++){
                for( int row =0 ; row < arr[0].length; row++){
                    System.out.print(arr[column][row]+ " ");
                }
            System.out.println();
            }

        } else if (format == false){
             // col

             for(int row =0 ; row < arr[0].length; row++){
                for( int column =0; column <  arr.length; column++){
                    System.out.print(arr[column][row]+ " ");
                }
            System.out.println();
            }
        }
        i = 2; 
        }


    }


    public static int [][] translate (int[][] arr){ // Column major to row major 
        // int temp [][] = new int [arr[0].length][arr.length]; 
        int translate [][] = new int [arr[0].length][arr.length]; 

        for(int row = 0 ; row < arr[0].length; row++){
            for( int column = 0; column <  arr.length; column++){
                translate[row][column] = arr[column][row]; 
            }
        }


        //translate = temp; 
        //System.out.println(Arrays.deepToString(translate));
        return translate; 
    }

    public static int [][] addMatrix( int[][] a, boolean formatA, int[][] b, boolean formatB) { // Add the Matrices 
        //if (formatA == false){translate(a); } // to Row if col
        if (formatB == false){
            //System.out.println(Arrays.deepToString(b));
            b=translate(b); 
            //System.out.println("new B");
            //System.out.println(Arrays.deepToString(b));
        } // to Row if col

        if (a.length != b.length || a[0].length != b[0].length){ // Checking if they can be added 
            System.out.println("the arrays cannot be added!");
            return null; 
        }

        // Adding 
        //int temp [][] = new int [a.length][a[0].length]; 
        int addMatrix [][] = new int [a.length] [a[0].length]; 
        for(int row = 0 ; row < a.length; row++){
            for( int column = 0; column <  a[0].length; column++){
                addMatrix[row][column] = a[row][column] + b[row][column]; 
            }
        }

        //addMatrix = temp; 
        return addMatrix; 

    }

    public static int RanNum2_10 (){ // Random Nnumber between 2 and 10
        return (int)(Math.random()*5 + 2);
    }
    
}