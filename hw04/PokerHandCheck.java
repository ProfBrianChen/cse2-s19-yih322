// HW 04
// CSE 002
// Link to the HW: 
// https://docs.google.com/document/d/1b-gavIEoWtG55WyLTYGg-Qmsuows9VGisa8bceeEpTk/edit

public class PokerHandCheck {
    public static void main(String[] args) {
        // Declearing the Random number generator 
        // Max of the random number 
        int RanMax = 52; 
        // Min of Random number 
        int RanMin = 1; 
        // Range of random numbers
        int RanRange = RanMax - RanMin +1; 
        // Generate the random numbers for 5 sets  
        int RanNum1 = (int)(Math.random() * RanRange) + RanMin; 
        int RanNum2 = (int)(Math.random() * RanRange) + RanMin;
        int RanNum3 = (int)(Math.random() * RanRange) + RanMin;
        int RanNum4 = (int)(Math.random() * RanRange) + RanMin;
        int RanNum5 = (int)(Math.random() * RanRange) + RanMin;

        // Determind card numbner regardless of kind
        // Find what number the card is within a kind
        int CardNum1 = RanNum1 % 13; 
        int CardNum2 = RanNum2 % 13;
        int CardNum3 = RanNum3 % 13;
        int CardNum4 = RanNum4 % 13;
        int CardNum5 = RanNum5 % 13;

        // Card Counter for same number regarless of kind
        int Ace = 0; 
        int Two = 0; 
        int Three = 0; 
        int Four = 0;
        int Five = 0;
        int Six = 0;
        int Seven = 0;
        int Eight = 0;
        int Nine = 0;
        int Ten = 0;
        int Jack = 0;
        int Queen = 0;
        int King = 0;



        // Card number and descirtion 
        // Determind the card ID regardless of class
        // Card Set 1
        String CardNumClass1 = new String(); 
        switch (CardNum1) {
            case 1: 
            CardNumClass1 = " Ace of ";
            Ace ++; 
            break; 

            case 2: 
            CardNumClass1 = " 2 of "; 
            Two ++; 
            break; 

            case 3: 
            CardNumClass1 = " 3 of "; 
            Three ++; 
            break; 

            case 4: 
            CardNumClass1 = " 4 of "; 
            Four ++; 
            break; 

            case 5: 
            CardNumClass1 = " 5 of "; 
            Five ++; 
            break; 

            case 6: 
            CardNumClass1 = " 6 of "; 
            Six ++; 
            break; 

            case 7: 
            CardNumClass1 = " 7 of "; 
            Seven ++; 
            break; 

            case 8: 
            CardNumClass1 = " 8 of "; 
            Eight ++; 
            break; 

            case 9: 
            CardNumClass1 = " 9 of ";
            Nine ++; 
            break; 

            case 10: 
            CardNumClass1 = " 10 of "; 
            Ten ++;
            break; 

            case 11: 
            CardNumClass1 = " Jack of "; 
            Jack ++;
            break; 

            case 12: 
            CardNumClass1 = " Queen of "; 
            Queen ++;
            break; 

            case 0: 
            CardNumClass1 = " King of "; 
            King ++;
            break; 


        }

        // Card Set 2
        String CardNumClass2 = new String(); 
        switch (CardNum2) {
            case 1: 
            CardNumClass2 = " Ace of ";
            Ace ++; 
            break; 

            case 2: 
            CardNumClass2 = " 2 of "; 
            Two ++; 
            break; 

            case 3: 
            CardNumClass2 = " 3 of "; 
            Three ++; 
            break; 

            case 4: 
            CardNumClass2 = " 4 of "; 
            Four ++; 
            break; 

            case 5: 
            CardNumClass2 = " 5 of "; 
            Five ++; 
            break; 

            case 6: 
            CardNumClass2 = " 6 of "; 
            Six ++; 
            break; 

            case 7: 
            CardNumClass2 = " 7 of "; 
            Seven ++; 
            break; 

            case 8: 
            CardNumClass2 = " 8 of "; 
            Eight ++; 
            break; 

            case 9: 
            CardNumClass2 = " 9 of ";
            Nine ++; 
            break; 

            case 10: 
            CardNumClass2 = " 10 of "; 
            Ten ++;
            break; 

            case 11: 
            CardNumClass2 = " Jack of "; 
            Jack ++;
            break; 

            case 12: 
            CardNumClass2 = " Queen of "; 
            Queen ++;
            break; 

            case 0: 
            CardNumClass2 = " King of "; 
            King ++;
            break; 

        }

        // Card Set 3
        String CardNumClass3 = new String(); 
        switch (CardNum3) {
            case 1: 
            CardNumClass3 = " Ace of ";
            Ace ++; 
            break; 

            case 2: 
            CardNumClass3 = " 2 of "; 
            Two ++; 
            break; 

            case 3: 
            CardNumClass3 = " 3 of "; 
            Three ++; 
            break; 

            case 4: 
            CardNumClass3 = " 4 of "; 
            Four ++; 
            break; 

            case 5: 
            CardNumClass3 = " 5 of "; 
            Five ++; 
            break; 

            case 6: 
            CardNumClass3 = " 6 of "; 
            Six ++; 
            break; 

            case 7: 
            CardNumClass3 = " 7 of "; 
            Seven ++; 
            break; 

            case 8: 
            CardNumClass3 = " 8 of "; 
            Eight ++; 
            break; 

            case 9: 
            CardNumClass3 = " 9 of ";
            Nine ++; 
            break; 

            case 10: 
            CardNumClass3 = " 10 of "; 
            Ten ++;
            break; 

            case 11: 
            CardNumClass3 = " Jack of "; 
            Jack ++;
            break; 

            case 12: 
            CardNumClass3 = " Queen of "; 
            Queen ++;
            break; 

            case 0: 
            CardNumClass3 = " King of "; 
            King ++;
            break; 

        }


        // Card Set 4
        String CardNumClass4 = new String(); 
        switch (CardNum4) {
            case 1: 
            CardNumClass4 = " Ace of ";
            Ace ++; 
            break; 

            case 2: 
            CardNumClass4 = " 2 of "; 
            Two ++; 
            break; 

            case 3: 
            CardNumClass4 = " 3 of "; 
            Three ++; 
            break; 

            case 4: 
            CardNumClass4 = " 4 of "; 
            Four ++; 
            break; 

            case 5: 
            CardNumClass4 = " 5 of "; 
            Five ++; 
            break; 

            case 6: 
            CardNumClass4 = " 6 of "; 
            Six ++; 
            break; 

            case 7: 
            CardNumClass4 = " 7 of "; 
            Seven ++; 
            break; 

            case 8: 
            CardNumClass4 = " 8 of "; 
            Eight ++; 
            break; 

            case 9: 
            CardNumClass4 = " 9 of ";
            Nine ++; 
            break; 

            case 10: 
            CardNumClass4 = " 10 of "; 
            Ten ++;
            break; 

            case 11: 
            CardNumClass4 = " Jack of "; 
            Jack ++;
            break; 

            case 12: 
            CardNumClass4 = " Queen of "; 
            Queen ++;
            break; 

            case 0: 
            CardNumClass4 = " King of "; 
            King ++;
            break; 

        }


        // Card Set 5
        String CardNumClass5 = new String(); 
        switch (CardNum5) {
            case 1: 
            CardNumClass5 = " Ace of ";
            Ace ++; 
            break; 

            case 2: 
            CardNumClass5 = " 2 of "; 
            Two ++; 
            break; 

            case 3: 
            CardNumClass5 = " 3 of "; 
            Three ++; 
            break; 

            case 4: 
            CardNumClass5 = " 4 of "; 
            Four ++; 
            break; 

            case 5: 
            CardNumClass5 = " 5 of "; 
            Five ++; 
            break; 

            case 6: 
            CardNumClass5 = " 6 of "; 
            Six ++; 
            break; 

            case 7: 
            CardNumClass5 = " 7 of "; 
            Seven ++; 
            break; 

            case 8: 
            CardNumClass5 = " 8 of "; 
            Eight ++; 
            break; 

            case 9: 
            CardNumClass5 = " 9 of ";
            Nine ++; 
            break; 

            case 10: 
            CardNumClass5 = " 10 of "; 
            Ten ++;
            break; 

            case 11: 
            CardNumClass5 = " Jack of "; 
            Jack ++;
            break; 

            case 12: 
            CardNumClass5 = " Queen of "; 
            Queen ++;
            break; 

            case 0: 
            CardNumClass5 = " King of "; 
            King ++;
            break; 

        }





        //System.out.println(CardNumClass);
        //---------------------------------------------------------------
        // Card Type Counter
        // Also determind what kind of card each 5 cards is
        int Diamonds = 0;
        int Clubs = 0;
        int Hearts = 0;
        int Spades = 0;
        //Determind what kind of card it is for all 5 set
        // Set 1
        String CardClass1 = new String(); 
        if (RanNum1 <= 13){ 
            //1-13, Diamonds
            CardClass1 = "Diamonds ";
            Diamonds++; 
        } else if ( RanNum1 >= 14 && RanNum1 <= 26){
            //14-26, clubs
            CardClass1 = "Clubs "; 
            Clubs ++;
        } else if ( RanNum1 >= 27 && RanNum1 <= 39){
            //27-39, hearts
            CardClass1 = "Hearts "; 
            Hearts ++; 
        } else if ( RanNum1 >= 40 && RanNum1 <= 52){
            //40-52, spades
            CardClass1 = "Spades "; 
            Spades ++; 
        }

        // Set 2
        String CardClass2 = new String(); 
        if (RanNum2 <= 13){ 
            //1-13, Diamonds
            CardClass2 = "Diamonds ";
            Diamonds ++; 
        } else if ( RanNum2 >= 14 && RanNum2 <= 26){
            //14-26, clubs
            CardClass2 = "Clubs "; 
            Clubs ++; 
        } else if ( RanNum2 >= 27 && RanNum2 <= 39){
            //27-39, hearts
            CardClass2 = "Hearts "; 
            Hearts ++;
        } else if ( RanNum2 >= 40 && RanNum2 <= 52){
            //40-52, spades
            CardClass2 = "Spades "; 
            Spades ++;
        }

        // Set 3
        String CardClass3 = new String();  
        if (RanNum3 <= 13){ 
            //1-13, Diamonds
            CardClass3 = "Diamonds ";
            Diamonds ++;
        } else if ( RanNum3 >= 14 && RanNum3 <= 26){
            //14-26, clubs
            CardClass3 = "Clubs "; 
            Clubs ++;
        } else if ( RanNum3 >= 27 && RanNum3 <= 39){
            //27-39, hearts
            CardClass3 = "Hearts "; 
            Hearts ++;
        } else if ( RanNum3 >= 40 && RanNum3 <= 52){
            //40-52, spades
            CardClass3 = "Spades "; 
            Spades ++;
        }

        // Set 4
        String CardClass4 = new String(); 
        if (RanNum4 <= 13){ 
            //1-13, Diamonds
            CardClass4 = "Diamonds ";
            Diamonds ++;
        } else if ( RanNum4 >= 14 && RanNum4 <= 26){
            //14-26, clubs
            CardClass4 = "Clubs "; 
            Clubs ++;
        } else if ( RanNum4 >= 27 && RanNum4 <= 39){
            //27-39, hearts
            CardClass4 = "Hearts "; 
            Hearts ++;
        } else if ( RanNum4 >= 40 && RanNum4 <= 52){
            //40-52, spades
            CardClass4 = "Spades "; 
            Spades++;
        }

        // Set 5
        String CardClass5 = new String(); 
        if (RanNum5 <= 13){ 
            //1-13, Diamonds
            CardClass5 = "Diamonds ";
            Diamonds ++;
        } else if ( RanNum5 >= 14 && RanNum5 <= 26){
            //14-26, clubs
            CardClass5 = "Clubs "; 
            Clubs ++; 
        } else if ( RanNum5 >= 27 && RanNum5 <= 39){
            //27-39, hearts
            CardClass5 = "Hearts "; 
            Hearts ++; 
        } else if ( RanNum5 >= 40 && RanNum5 <= 52){
            //40-52, spades
            CardClass5 = "Spades "; 
            Spades ++; 
        }

        //-----------------------------------------------
        // Checking the number of appreances regarding each card number, 1->13
        // Any triples or pairs? 
        int Triple = 0; 
        int Pairs = 0; 
        switch (Ace) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }

        switch (Two) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }

        switch (Three) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Four) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Five) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Six) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Seven) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
    
        switch (Eight) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Nine) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Ten) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Jack) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (Queen) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }
                
        switch (King) {
            case 2: 
            Pairs ++; 
            break; 

            case 3: 
            Triple ++;  
            break; 

        }

        // Any three of its kinds? 
        boolean ThreeOfKind = false; 
        switch (Diamonds){
            case 3: 
            ThreeOfKind = true; 
            break; 
        }

        switch (Clubs){
            case 3: 
            ThreeOfKind = true; 
            break; 
        }

        switch (Hearts){
            case 3: 
            ThreeOfKind = true; 
            break; 
        }

        switch (Spades){
            case 3: 
            ThreeOfKind = true; 
            break; 
        }
                
        

        // Print out results
        System.out.println("Your 5 random cards are: ");
        System.out.println(CardNumClass1 + CardClass1);
        System.out.println(CardNumClass2 + CardClass2);
        System.out.println(CardNumClass3 + CardClass3);
        System.out.println(CardNumClass4 + CardClass4);
        System.out.println(CardNumClass5 + CardClass5);


        // Check for conditions 
        // 1 pair and 1 triple? 
        // 1 pair at all? 
        // 2 pairs? 
        // 1 triple? 
        // High hand for getting no luck
        if (ThreeOfKind ==  true){
            System.out.println("You have a three of its kind!!!");
        }   else if (Triple == 1) {
            System.out.println("You have a triple!!!");
        }   else if (Pairs == 2) {
            System.out.println("You have two pair!!!");
        }   else if (Pairs == 1) {
            System.out.println("You have one pairs!!!");
        }   else {
            System.out.println("You have a high card hand!");
        }

    }
}