

public class TestingPattern {
    public static void main(String[] args) {
        int length = 10; 

         // Declear Counter i and j
         int i = 0; // Row
         int j = 0; // Column
         int a = 0; 
 
         // Loop the Pattern 
         for (i = 0; i <= length; i++){
            
            for (j = i; j < length; j++){

                System.out.print(" ");
            }


            for (j = i; j > 0; j--){

                System.out.print(j);  
            }
    
            System.out.println("");
    
        } 

    }
}