/* 
                           1
                          21
                         321
                        4321
                       54321
                      654321

*/ 

// Input only Interger from 1-10

// Import Scanner 
import java.util.Scanner;


public class PatternC {
    public static void main(String[] args) {
        // Declear Scanner 
        Scanner NumInput = new Scanner(System.in); 

        // Ask for an input 
        System.out.print("Enter only positive intergter from 1 - 10: ");

         // Check for if it is an int
         int length = 0; 

         while (true) {
            // if it is an int
            if (NumInput.hasNextInt()){
                length = NumInput.nextInt(); 

                // if it is a positive 
                if (length > 0 && length <= 10){
                    break;   
                }   else {
                    // negative case 
                    System.out.println("Wrong entry, Enter only positive intergter from 1 - 10: ");
                }

            }   else {
                // not an int
                System.out.println("Wrong entry, Enter only positive intergter from 1 - 10: ");
                // Trash the bad input 
                String junkWord = NumInput.next(); 
            }
        }
        

        // Declear Counter i and j
        int i = 0; // Row
        int j = 0; // Column

        // Loop the Pattern 

         for (i = 0; i <= length; i++){
            
            for (j = i; j < length; j++){

                System.out.print(" ");
            }


            for (j = i; j > 0; j--){

                System.out.print(j);  
            }
    
            System.out.println("");
    
        } 




    }
}