// cSE 002 
// Area 
// https://docs.google.com/document/d/1irNIElpPwDJwVKw7MfAd9HIafwmovUmT6XRhoTajmf8/edit

// Set up scanner 
import java.util.Scanner;


public class Area {

    // Method for checking right number inputs for doubles

    public static double CheckDoubles (double input) {
        // Declear values and scanner 
        Scanner numInput = new Scanner(System.in); 
        double num = 0; 
        while (true){
            if (numInput.hasNextDouble()){
                num = numInput.nextDouble(); 
                if (num > 0){
                    // Check for postitive 
                    break;
                } else {
                    System.out.println("Please enter the a postive number: ");
                }
            }   else {
                // Not the right type
                System.out.println("Please enter the a postive number: ");
            }
            // Trash the bad input 
            String junkWord = numInput.next();             
        }
        return num; 
    }

    public static double rectangle (Double Rectangle){
        // Scanner and declare values & ask for the input and check them

        double length = 0, width = 0, rectangleArea = 0; 
        System.out.println("Please enter the length: ");
        length = CheckDoubles(length); 

        System.out.println("Please enter the width: ");
        width = CheckDoubles(width); 

        rectangleArea = length * width;
        return rectangleArea; 
    }

    public static double triangle (Double Triangle){
        // Scanner and declare values & ask for the input and check them
        double base = 0, height = 0, triangleArea = 0; 
        System.out.println("Please enter the base: ");
        base = CheckDoubles(base); 

        System.out.println("Please enter the height: ");
        height = CheckDoubles(height); 
        
        triangleArea = base * height; 
        return triangleArea; 
    }

    public static double circle (Double Circle){
        // Scanner and declare values & ask for the input and check them
        Scanner specInput = new Scanner(System.in); 
        double radius = 0, circleArea = 0; 
        System.out.println("Please enter the radius: ");
        radius = CheckDoubles(radius); 


        circleArea =  Math.PI * radius * radius; 
        return circleArea; 
    }
    public static void main(String[] args) {
        double area = 0; 
        // Scanner 
        Scanner input = new Scanner(System.in); 

        // Declear values
        String shape = " ";
        int n = 0;
        System.out.println("Please enter the right shape, rectangle, or triangle, or circle");
        
        while ( n < 1){
            shape = input.next(); 
            if (shape.equals("rectangle") || shape.equals("triangle") || shape.equals("circle") ){
                n =5; 
                break; 
            } else {
                System.out.println("Please enter the right shape, rectangle, or triangle, or circle");
            }

        } 

        switch (shape){
            case "rectangle": 
            area = rectangle(2.0); 
            break; 

            case "triangle": 
            area = triangle(2.1);
            break; 

            case "circle": 
            area = circle(2.2); 
            break; 

            default: 
            System.out.println("Error");
            break; 
        }
        
        System.out.println("The area of the " + shape + " is " + area);



    }
}