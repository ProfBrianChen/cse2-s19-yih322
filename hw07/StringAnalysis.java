// CSE 002 HW 07 Q2
// https://docs.google.com/document/d/1irNIElpPwDJwVKw7MfAd9HIafwmovUmT6XRhoTajmf8/edit

// import scanner
import java.util.Scanner; 

public class StringAnalysis {

    // Check the input type if it is a positive integer 
    static int CheckPosInt (int inputByUser) {
        Scanner NumInput = new Scanner(System.in); 

        // Check for right type of input 
        while (true) {
           // if it is an int
           if (NumInput.hasNextInt()){
            inputByUser = NumInput.nextInt(); 

               // if it is a positive 
               if (inputByUser > 0){
                   break;   
               }   else {
                   // negative case 
                   System.out.println("Error: please type in an positive integer: ");
               }

           }   else {
               // not an int
               System.out.println("Error: please type in an positive integer: ");
               // Trash the bad input 
               String junkWord = NumInput.next(); 
           }
       }   

       return inputByUser; 
    }
    
    private static boolean CheckLetter (String input) {
        // Declear values
        boolean allLetter = true; 
        // length of the string 
        int sLength = input.length(); 
        // Check if it is all letter within the range
        for (int i = 0; i < sLength; i++){

            if ( Character.isLetter(input.charAt(i)) ){
                allLetter = true;
            } else {
                allLetter = false; 
                break; 
            }
        }

        
        return allLetter; 
    }

    private static boolean CheckLetter (String input, int charnum) {
        // Declear values
        boolean allLetter = true; 
        // length of the string 
        int sLength = input.length();
        int length = 0; 
        // Chooice the length
        if (charnum >= sLength){
            length = sLength;
        }   else {
            length = charnum; 
        }

        // Check if it is all letter within the range
        for (int i = 0; i < length; i++){

            if ( Character.isLetter(input.charAt(i)) ){
                allLetter = true;
            } else {
                allLetter = false; 
                break; 
            }
        }
        
        return allLetter; 
    }

    public static void main(String[] args) {
        // Declear values and scanner 
        Scanner input = new Scanner(System.in);
        boolean allLetter = true; 
        String UserString ; 

        // ask the user to check the whole string or just just a specificed number of characters in the string
        int toRun = 0; 
        int toCheck = 0; 
        System.out.println("Enter 1 to check all the char; Enter 2 to check the specified number of char"); 
        toRun = CheckPosInt(toRun); 

        System.out.println("Enter the String: "); 

        switch (toRun){
            case 1: 
            UserString = input.next();
            allLetter = CheckLetter(UserString);
            break; 

            case 2: 
            UserString = input.next();
            System.out.println("How many char do you want to check?: "); 
            toCheck = CheckPosInt(toCheck); 
            allLetter = CheckLetter(UserString, toCheck);
            break; 
        }

        // the output
        if (allLetter == true){
            System.out.println("The char check are all letters");
        } else if (allLetter == false){
            System.out.println("The char check are NOT all letters");
        }
        
    }
}