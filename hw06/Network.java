// HW 06 Boxes
// CSE 002 
// Due March 19th 

// Import Scanner 
import java.util.Scanner;

public class Network    {

    // Check the input type if it is a positive integer 
    static int CheckInt (int inputByUser) {
        Scanner NumInput = new Scanner(System.in); 
        // Check for riught type of input 
        while (true) {
           // if it is an int
           if (NumInput.hasNextInt()){
            inputByUser = NumInput.nextInt(); 
               // if it is a positive 
               if (inputByUser > 0){
                   break;   
               }   else {
                   // negative case 
                   System.out.println("Error: please type in an positive integer: ");
               }
            }   else {
               // not an int
               System.out.println("Error: please type in an positive integer: ");
               // Trash the bad input 
               String junkWord = NumInput.next(); 
           }
       }   
       return inputByUser; 
    }
    
    public static void main(String[] args) {

        // Declear var and get inputs
        int height = 0, width = 0, SquareSize = 0, EdgeLength = 0; 
        System.out.println("Please provide the height as a positive integer: ");
        height = CheckInt(height); 
        System.out.println("Please provide the width as a positive integer: ");
        width = CheckInt(width); 
        System.out.println("Please provide the SquareSize as a positive integer: ");
        SquareSize = CheckInt(SquareSize); 
        System.out.println("Please provide the EdgeLength as a positive integer: ");
        EdgeLength = CheckInt(EdgeLength); 


        // Determind if the SquareSize # is odd or even 
        boolean EvenSize = false; 
        if ( SquareSize % 2 == 0){
            EvenSize = true; 
        }   else {
            EvenSize = false; 
        }

        // One unit = one boxes + all the edges 
        int UnitLength =  SquareSize + EdgeLength; 
        // For drawing boxes
        for (int column = 1; column <= height; column++){
            for (int row = 1; row <= width; row++){
                // Printing all the corners
                if (row == 1 && column == 1 || row == SquareSize && column == SquareSize || row == 1 && column == SquareSize || row == SquareSize && column == 1 || (column % UnitLength) == 1 && (row % UnitLength) == 1 || column % UnitLength == SquareSize && (row % UnitLength) == 1 || column % UnitLength == 1 && (row % UnitLength) == SquareSize || column % UnitLength == SquareSize && (row % UnitLength) == SquareSize){
                    System.out.print("#");
                } else if ( (column % UnitLength) == SquareSize && ((row-1) % UnitLength) < SquareSize || column == 1 && ((row-1) % UnitLength) < SquareSize || (column % UnitLength) == 1 && ((row-1) % UnitLength) < SquareSize){
                    // Print the hor. side, print them only if they are witnin the range of the rectangle 
                    System.out.print("-");
                } else if ( (row % UnitLength) == SquareSize && ((column-1) % UnitLength) < SquareSize || row == 1 && ((column-1) % UnitLength) < SquareSize || (row % UnitLength) == 1 && ((column-1) % UnitLength) < SquareSize){
                    // Print Vertical side, print them only if they are witnin the range of the rectangle 
                    System.out.print("|");

                } else if (EvenSize && column >= (SquareSize/2) && column <= (SquareSize/2)+1 && row > SquareSize && row <= UnitLength || EvenSize && (column % UnitLength) >= (SquareSize/2) && (column % UnitLength) <= (SquareSize/2)+1 && ((row) % UnitLength) > SquareSize && ((row) % UnitLength) <= UnitLength || EvenSize && (column % UnitLength) >= (SquareSize/2) && (column % UnitLength) <= (SquareSize/2)+1 && row % UnitLength == 0){
                    // If the the size is even, print double edges 
                    // Hoizontal
                     System.out.print("-");
                } else if (EvenSize && row >= (SquareSize/2) && row <= (SquareSize/2)+1 && column > SquareSize && column <= UnitLength || EvenSize && (row % UnitLength) >= (SquareSize/2) && (row % UnitLength) <= (SquareSize/2)+1 && ((column) % UnitLength) > SquareSize && ((column) % UnitLength) <= UnitLength || EvenSize && (row % UnitLength) >= (SquareSize/2) && (row % UnitLength) <= (SquareSize/2)+1 && column % UnitLength == 0){
                     // Vertical 
                     System.out.print("|");

                } else if (EvenSize == false && column == (SquareSize/2)+1 && row > SquareSize && row <= UnitLength || EvenSize == false && (column % UnitLength) == (SquareSize/2)+1 && ((row) % UnitLength) > SquareSize && ((row) % UnitLength) <= UnitLength || EvenSize == false && (column % UnitLength) == (SquareSize/2)+1 && row % UnitLength == 0) {
                    // If the the size is odd, print double edges 
                    // Hoizontal if odd 
                    System.out.print("-");
                } else if (EvenSize == false && row == (SquareSize/2)+1 && column > SquareSize && column <= UnitLength || EvenSize == false && (row % UnitLength) == (SquareSize/2)+1 && ((column) % UnitLength) > SquareSize && ((column) % UnitLength) <= UnitLength || EvenSize == false && (row % UnitLength) == (SquareSize/2)+1 && column % UnitLength == 0) {
                     // Vertical if odd
                     System.out.print("|");                    
                } else {
                     // The blank space
                     System.out.print(" ");
                }
            }
            // Next line
            System.out.println();

        }
        
    }
}
