// Extra Credit Argyle 
// CSE 002 
// 03-17-2019 

// Import Scanner 
import java.util.Scanner;; 

public class Argyle {
    
    // Check the input type if it is a positive integer 
    static int CheckPosInt (int inputByUser) {
        Scanner NumInput = new Scanner(System.in); 

        // Check for right type of input 
        while (true) {
           // if it is an int
           if (NumInput.hasNextInt()){
            inputByUser = NumInput.nextInt(); 

               // if it is a positive 
               if (inputByUser > 0){
                   break;   
               }   else {
                   // negative case 
                   System.out.println("Error: please type in an positive integer: ");
               }

           }   else {
               // not an int
               System.out.println("Error: please type in an positive integer: ");
               // Trash the bad input 
               String junkWord = NumInput.next(); 
           }
       }   

       return inputByUser; 
    }

    static int CheckArgyleStripe (int OddinputByUser, int DiamondsWidth){
        // Declear Scanner 
        Scanner OddNumInput = new Scanner(System.in); 
        // Check for if it is an odd number and half of the size of the DiamondsWidth
        while (true){
            if (OddNumInput.hasNextInt()){
                OddinputByUser = OddNumInput.nextInt(); 
                // Check for conditions 
                if (OddinputByUser > 0 && OddinputByUser % 2 == 1 && OddinputByUser < (DiamondsWidth / 2)){
                    break; // Condition stasified 
                } else {
                    // negative case 
                    System.out.println("Error: please type in an positive odd integer: ");
                }

            }   else {
                // not an int
                System.out.println("Error: please type in an positive odd integer: ");
                // Trash the bad input 
                String junkWord = OddNumInput.next(); 
            }
        }
        return OddinputByUser; 
    }

    
    public static void main(String[] args) {
        
        // Declear var and get inputs
        int width = 0, height = 0, DiamondsWidth = 0, CenterStripeWidth = 0; 
        System.out.println("Please provide a positive integer for the width of Viewing window in characters: ");
        width = CheckPosInt(width); 
        System.out.println("Please provide a positive integer for the height of Viewing window in characters: ");
        height = CheckPosInt(height); 
        System.out.println("Please provide positive integer for the width of the argyle diamonds: ");
        DiamondsWidth = CheckPosInt(DiamondsWidth); 
        System.out.println("Please provide a positive odd integer for the width of the argyle center stripe: ");
        CenterStripeWidth = CheckArgyleStripe(CenterStripeWidth, DiamondsWidth); 
        // Get char
        Scanner charInput = new Scanner(System.in);
        // Fisrt Char 
        System.out.println("Please provide the first character for the pattern fill: "); 
        String FisrtPatternInput = charInput.next(); 
        char FisrtPattern = FisrtPatternInput.charAt(0); 
        // Second Char
        System.out.println("Please provide the second character for the pattern fill: ");
        String SecondPatternInput = charInput.next();
        char SecondPattern = SecondPatternInput.charAt(0); 
        // Third Char
        System.out.println("Please provide the third character for the pattern fill: ");
        String ThirdPatternInput = charInput.next();
        char ThirdPattern = ThirdPatternInput.charAt(0); 

        
        System.out.println(width);
        System.out.println(height);
        System.out.println(DiamondsWidth);
        System.out.println(CenterStripeWidth);
        System.out.println(FisrtPattern);
        System.out.println(SecondPattern);
        System.out.println(ThirdPattern);



    }
}