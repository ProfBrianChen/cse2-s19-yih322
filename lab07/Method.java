// CSE 002 Lab 7
// https://docs.google.com/document/d/1KNtYHqMG7MmdE3qQdfKKakgy5Ug3YrO2dcFN5L1fLDk/edit
// 03/22/2019

// Import Method for random and scanner
import java.util.Random;
import java.util.Scanner;

public class Method {
    public static String Adj(int ranNum){
        String Adj = " "; 
        switch (ranNum){
            case 0: 
            Adj = "boring"; 
            break; 

            case 2: 
            Adj = "descriptive"; 
            break; 

            case 3: 
            Adj = "rabid"; 
            break; 
    
            case 4: 
            Adj = "overwrought"; 
            break; 
    
            case 5: 
            Adj = "odd"; 
            break; 
    
            case 6: 
            Adj = "spotless"; 
            break; 
    
            case 7: 
            Adj = "sloppy"; 
            break; 
    
            case 8: 
            Adj = "judicious"; 
            break; 
    
            case 9: 
            Adj = "dear"; 
            break; 
            
            default: 
            Adj = "old";  

        } 
        return Adj; 
    }

    public static String Sub(int ranNum){
        String Sub = " "; 
            
        switch (ranNum){
            case 0: 
            Sub = "dog"; 
            break; 

            case 2: 
            Sub = "cat"; 
            break; 

            case 3: 
            Sub = "fish"; 
            break; 
    
            case 4: 
            Sub = "fox"; 
            break; 
    
            case 5: 
            Sub = "cow"; 
            break; 
    
            case 6: 
            Sub = "pig"; 
            break; 
    
            case 7: 
            Sub = "dragon"; 
            break; 
    
            case 8: 
            Sub = "rat"; 
            break; 
    
            case 9: 
            Sub = "bird"; 
            break; 

            default: 
            Sub = "banna";  
            
        } 
        return Sub; 
    }

    public static String ver(int ranNum){
        String ver = " "; 
        switch (ranNum){
            case 0: 
            ver = "went"; 
            break; 

            case 2: 
            ver = "ran"; 
            break; 

            case 3: 
            ver = "slept"; 
            break; 
    
            case 4: 
            ver = "beat"; 
            break; 
    
            case 5: 
            ver = "texted"; 
            break; 
    
            case 6: 
            ver = "called"; 
            break; 
    
            case 7: 
            ver = "typed"; 
            break; 
    
            case 8: 
            ver = "throw"; 
            break; 
    
            case 9: 
            ver = "killed"; 
            break; 

            default: 
            ver = "ate";            
            
        } 
        return ver; 
    }

    public static String obj(int ranNum){
        String obj = " "; 
        switch (ranNum){
            case 0: 
            obj = "ball"; 
            break; 

            case 2: 
            obj = "penis"; 
            break; 

            case 3: 
            obj = "vagina"; 
            break; 
    
            case 4: 
            obj = "vulva"; 
            break; 
    
            case 5: 
            obj = "key"; 
            break; 
    
            case 6: 
            obj = "pen"; 
            break; 
    
            case 7: 
            obj = "trash"; 
            break; 
    
            case 8: 
            obj = "case"; 
            break; 
    
            case 9: 
            obj = "phone"; 
            break; 

            default: 
            obj = "TV"; 
            
        } 
        return obj; 
    }


    public static String FirstSen(Boolean Cont){
        String Adjectives, Adjectives2, subject, subject2, pastVerb, object ,FirstSen; 
        // This statement generates random integers less than 10
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        // 1st set of adj and sub
        Adjectives = Adj(randomInt); 
        subject = Sub(randomInt);

        // 2nd set of adj and sub
        randomInt = randomGenerator.nextInt(10);
        Adjectives2 = Adj(randomInt); 
        subject2 = Sub(randomInt); 
        pastVerb = ver(randomInt); 
        object = obj(randomInt); 
        FirstSen = ("The " + Adjectives + " " + subject + " " + pastVerb + " the " + Adjectives2 + " " + object + ". "); 
        System.out.print(FirstSen); 

        return subject; 
    }

    public static String RestPara (String sub){
        String subjectA = sub; 
        String AdjectivesS, AdjectivesC, subjectS, subjectC, pastVerb, object, SupSen, Concl, RestPara; 
        // for Supporting Sen
        // This statement generates random integers less than 10
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        AdjectivesS = Adj(randomInt);
        object = obj(randomInt); 
        pastVerb = ver(randomInt); 
        subjectS = Sub(randomInt); 
        SupSen = ("It used " + object + " to " + pastVerb + " " + subjectS + "and then took an L. ");

        // Conclusion 
        randomInt = randomGenerator.nextInt(10);
        AdjectivesS = Adj(randomInt);
        object = obj(randomInt); 
        pastVerb = ver(randomInt); 
        subjectS = Sub(randomInt); 
        randomInt = randomGenerator.nextInt(10);
        subjectC = Sub(randomInt); 
        AdjectivesC = Adj(randomInt);
        Concl = ("At the end, the " + subjectA + " " + pastVerb + " the " + AdjectivesS + " " + subjectS + " and the " + AdjectivesC + " " +subjectC + ". "); 

        // The rest 
        RestPara = SupSen + Concl; 

        return RestPara; 
    }
    public static void main(String[] args) {
        // Scanner 
        Scanner checkInput = new Scanner(System.in); 
        int Checkin = 0; 
        // First Sentence 
        String FirstSen = " "; 
        String subject = " "; 
        String RestPara = " "; 
        
        // ask the user if were to continue
        boolean Cont = true;  
        while (Cont) {
            subject = FirstSen(Cont); 
            //System.out.println(FirstSen); 
            RestPara = RestPara(subject); 
            System.out.println(RestPara); 

            System.out.println("Enter 1 to continue, or 0 to stop"); 
            Checkin = checkInput.nextInt(); 
            if (Checkin == 0){
                Cont = false; 
                System.out.println("GG"); 
            }
        }

    }
}
