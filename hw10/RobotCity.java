// Robo Ciy @ https://docs.google.com/document/d/1SmIUTdUoHUf1-gPCyyc9NlLZ8XJPKkjrxHcEreWlmFA/edit
import java.util.Arrays;

public class RobotCity {
    public static void main(String[] args) {
        int cityPop [][] = buildCity(); 
        for (int i = 0; i < 5; i++){
            cityPop = buildCity();
            System.out.println("------------------------------ City # " + i);
            PrintPop(cityPop);
            int k = RanNumWithRange(2, 10); // 2 to 10 robots
            System.out.println("------------------------------ # of Robot invaded the city " + k);
            cityPop = invade(cityPop, k);
            PrintPop(cityPop);
            System.out.println("------------------------------ The robots moved east by a block ");
            cityPop = update(cityPop);         
            PrintPop(cityPop);
        }
    }
    public static int [][] buildCity (){    // build city
        int StoN = RanNumWithRange(10, 15); int WtoE = RanNumWithRange(10, 15); // Size of the city
        System.out.println("The size of the city is: " + StoN + " * " + WtoE + " (South-North * West-East).  ");
        int cityPop [][] = new int [StoN][WtoE]; // Fill in the city with populations 
        for (int i = 0; i < StoN; i++){
            for (int j = 0; j < WtoE; j++){
                cityPop[i][j] = RanNumWithRange(100, 999); // Each Block pop range from 100-900
            }   }
            return cityPop; 
    }
    public static int [][] invade(int [][] cityPop, int k){
        int invadedCounter = 0; int i = 0; int j = 0; 
        while (invadedCounter < k){
            i = RanNumWithRange(0, cityPop.length -1); j =  RanNumWithRange(0, cityPop[0].length -1);
            if (cityPop[i][j] > 0){ // Only invade if it hasn't been invaded 
                cityPop[i][j] = -(cityPop[i][j]); 
                invadedCounter++; 
            }
        }
        return cityPop; 
    }
    public static int [][] update(int [][] cityPop){ // Robot to move east
        for (int i = cityPop.length -1; i >= 0 ; i--){
            for (int j = cityPop[0].length -1; j >= 0 ; j--){
                if (cityPop[i][j] < 0 && j != cityPop[0].length -1) {  
                    cityPop[i][j] = -(cityPop[i][j]);
                    cityPop[i][j+1] = -(cityPop[i][j+1]);
                } else if (cityPop[i][j] < 0 && j == cityPop[0].length -1){
                    cityPop[i][j] = -(cityPop[i][j]);
                }
            }   
        }
        return cityPop; 
    }

    public static void PrintPop (int [][] cityPop){ // Print City 
        for (int i = 0; i < cityPop.length; i++){
            for (int j = 0; j < cityPop[0].length; j++){
                System.out.print(cityPop[i][j] + " ");}
            System.out.println();}
    }

    public static int RanNumWithRange (int lower, int upper){ // Random Num Gen
        return (int)(Math.random() * (upper - lower + 1)  + lower); 
    }
}