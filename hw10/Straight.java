// CSE 002 HW 010 @ https://docs.google.com/document/d/1SmIUTdUoHUf1-gPCyyc9NlLZ8XJPKkjrxHcEreWlmFA/edit
import java.util.Arrays;

public class Straight {
    public static void main(String[] args) {
        long startTime = System.nanoTime(); // Check for Run time 
        int SedDeck [] = shuffledDeck(); 
        int First5 [] = drawFirst5(SedDeck); 
        int StraightCounter = 0; 

        for (int i = 0; i < 1000000; i++){
            SedDeck = shuffledDeck(); 
            First5  = drawFirst5(SedDeck); 
            if (searchForStraight(First5) == true){
                StraightCounter++; 
            }
        }
        double possb = StraightCounter / 1000000.0 * 100.0; 
        System.out.println(possb + " %");

        long endTime   = System.nanoTime();
        double totalTime = endTime - startTime;
        System.out.println((double)(totalTime/1000000000) + " sec");
    }

    public static boolean searchForStraight (int [] First5){
        //int [] NumOnly = new int [First5.length]; 

        boolean searchForStraight = false; 
        if (kLowestCard(First5, 1) + 1 == kLowestCard(First5, 2) && kLowestCard(First5, 1) + 2 == kLowestCard(First5, 3) && kLowestCard(First5, 1) + 3 == kLowestCard(First5, 4) && kLowestCard(First5, 1) + 4 == kLowestCard(First5, 5)){
            searchForStraight = true; 
        }
        return searchForStraight; 
    }
    public static int kLowestCard (int [] First5, int k){               // Translate them to numbers only 
        for (int i = 0; i < First5.length; i++){
            if (First5[i] > 12){
                First5[i] = First5[i] % 13; 
            }
        }
        Arrays.sort(First5);
        if (k > 5 || k < 0){
            System.out.println("Error");
            return -1; 
        }
        return First5[k-1]; 
    }

    public static int [] drawFirst5 (int [] shuffledDeck){
        int drawFirst5 [] = new int [5];
        for (int i = 0; i < drawFirst5.length; i++){
            drawFirst5[i] = shuffledDeck[i]; 
        }
        return drawFirst5; 
    }
    public static int [] shuffledDeck (){   // Create the shuffled deck, ans sort it
        int shuffledDeck [] = new int [52];
        for(int i = 0; i < shuffledDeck.length; i++){
            shuffledDeck [i] = RanNumWithRange(0, 52);
            if(i > 0){
                for(int c = 0;c <= i-1; c++){
                    if(shuffledDeck [c] == shuffledDeck [i]){
                        i--;}}}
        }
        return shuffledDeck; 
    }

    public static int RanNumWithRange (int lower, int upper){ // Random Num Gen
        return (int)(Math.random() * (upper - lower + 1)  + lower); 
    }
}