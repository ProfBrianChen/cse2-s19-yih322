// Import Scanner
// BoxVolume.java
import java.util.Scanner;

public class BoxVolume {
    public static void main(String[] args) {
        // Set up scanner
        Scanner newScanner = new Scanner(System.in); 

        // Set up for inputing W,L,H of the box
        // W
        System.out.print("Enter the width of the box: ");
        double width = newScanner.nextDouble(); 

        // L
        System.out.print("Enter the length of the box: ");
        double length = newScanner.nextDouble(); 

        // H
        System.out.print("Enter the height of the box: ");
        double height = newScanner.nextDouble(); 

        // Volume = W*L*M
        double volume = width * length * height; 

        System.out.println("The volume of the box with width of " + width + ", " + "length of " + length + " and length of " + height + " is " + volume);



    }
}