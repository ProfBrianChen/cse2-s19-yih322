// Import Scanner
import java.util.Scanner;

// HW03 
// Convert.java
// CSE02

// Import Scanner

public class Convert {
    public static void main(String[] args) {
        // Declear Scanners
        Scanner newScanner = new Scanner(System.in); 
        System.out.println("Looks like you would like to convert meters to inches");
        System.out.print("Enter the number of meter you would like to convert: ");
        // Enter value in meter
        double meters =  newScanner.nextDouble(); 
        // 1 m = 39.3701
        double mtoin = 39.3701; 
         // Convert meter to inches
        double inches = meters * mtoin; 

        // Output
        System.out.println( meters + " meters is " + inches);

    }
}
