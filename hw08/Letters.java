// CSE 002 
// https://docs.google.com/document/d/1kJNygj6uCxs7N0Mf2ZQ1mg2CK30dOrNu_X_GQVrmUfw/edit


public class Letters {
    public static void main(String[] args) {

        // Array with random number that correspond to a random char with a random size 
        // Random size of the array
        int ranNumSize = Random1_20(1); 
        int [] ranNum = new int [ranNumSize]; 
        // Fill in this ranNum with random number that correspond to a random char according to the A
        ranNum = ranNumtoChar(ranNumSize);


        // Count how many A-M and N-Z in a index
        int AMCOUNTER = 0; int NZCounter = 0; 
        for (int i = 0; i < ranNumSize; i++) {
            if (ranNum [i] >= 65 && ranNum [i] <= 77 || ranNum [i] >= 97 && ranNum [i] <= 109 ) { // A-M CASE 
                AMCOUNTER++; 
            } else if (ranNum [i] >= 78 && ranNum [i] <= 90 || ranNum [i] >= 110 && ranNum [i] <= 122) { // NZ
                NZCounter++; 
            }
        }
        // Declear AM & NZ array 
        int [] AM = new int [AMCOUNTER]; int [] NZ = new int [NZCounter];
        AM = getAtoM(ranNum, AMCOUNTER); 
        NZ = getNtoZ(ranNum, NZCounter); 

        // Turn the OG "int" array to "char" array 
        char [] ranNumchar = new char [ranNumSize]; char [] AMchar = new char [AMCOUNTER]; char [] NZchar = new char [NZCounter];
        for (int i = 0; i < ranNumSize - 1; i++){
            ranNumchar [i] = (char) ranNum [i]; 
        } 
        for (int i = 0; i < AMCOUNTER - 1; i++){
            AMchar [i] = (char) AM [i]; 
        } 
        for (int i = 0; i < NZCounter - 1; i++){
            NZchar [i] = (char) NZ [i]; 
        } 

        // print out the arrays
        System.out.println("The number of the indexes: " + ranNumSize);
        System.out.println("Random character array: ");
        for (int i = 0; i < ranNumSize; i++){
            System.out.print(ranNumchar[i]);
        }
        System.out.println();
        System.out.print("AtoM characters: ");
        for (int i = 0; i < AM.length-1; i++){
            System.out.print(AMchar[i]);
        }
        System.out.println();
        System.out.print("NtoZ characters: ");
        for (int i = 0; i < NZ.length-1; i++){
            System.out.print(NZchar[i]);
        }
        System.out.println();

        System.out.println(ts); 
        
    }

    // Put the AM into a new array 
    public static int [] getAtoM (int [] array, int AMarrayLength){
        int [] getAtoM = new int [AMarrayLength]; 
        int getAtoM_Counter = 0; 
        for (int i = 0; i < array.length - 1; i++){
            if (array [i] >= 65 && array [i] <= 77 || array [i] >= 97 && array [i] <= 109){
                getAtoM [getAtoM_Counter] = array [i]; // If it is a AM put it into the AM array
                getAtoM_Counter++; 
            }
        }
        return getAtoM; 
    }

    // Put the NZ into a new array 
    public static int [] getNtoZ (int [] array, int NZarrayLength){
        int [] getNtoZ = new int [NZarrayLength]; 
        int getNtoZ_Counter = 0; 
        for (int i = 0; i < array.length - 1; i++){
            if (array [i] >= 78 && array [i] <= 90 || array [i] >= 110 && array [i] <= 122){
                getNtoZ [getNtoZ_Counter] = array [i]; // If it is a NZ put it into the AM array
                getNtoZ_Counter++; 
            }
        }
        return getNtoZ; 
    }   

    // Random Num gen, for creating the length of the array between 10-30
    public static int Random1_20 (int AValue){
        int Random0_20 = (int)(Math.random() * 20 + 10);
        return Random0_20;
    }

    // Fill in the array with of a certain size with random char that correspond to a letter upper case or lower case 
    public static int [] ranNumtoChar (int arrayLength){
        // Declear the output array 
        int [] ranNumtoChar = new int [arrayLength]; 
        // 0 for UPPER case, 1 for lower case
        int Random0_1 = 0;
        // Fill in the input array 
        for (int i = 0; i < arrayLength; i++){ 
            Random0_1 = (int)(Math.random() * 1 + 0.5);
            if (Random0_1 == 0){ // UPPER CASE 
                ranNumtoChar [i] = (int)(Math.random() * 26 + 65); 
            } else {
                ranNumtoChar [i] = (int)(Math.random() * 26 + 97); 
            }
        }
        return ranNumtoChar;
    }

}