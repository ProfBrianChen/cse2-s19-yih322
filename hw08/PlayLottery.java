// PlayLottery
// CSE 002 
// https://docs.google.com/document/d/1kJNygj6uCxs7N0Mf2ZQ1mg2CK30dOrNu_X_GQVrmUfw/edit
// Input Scanner 
import java.util.Scanner;
import java.util.Arrays;

public class PlayLottery {
    public static void main(String[] args) {
        // Declare scanner and array for user input
        Scanner inputByUser = new Scanner(System.in); 
        int [] UserIn = new int [5]; 
        System.out.println("Enter 5 numbers between 0 and 59: ");
        for (int i = 0; i < 5; i++){ // ask user to fill in the num array 
            System.out.println("Enter the #" + (i+1) + " number: ");
            UserIn [i] = inputByUser.nextInt();
        }
        // Genareate random array
        int [] Lnum = new int [5]; 
        Lnum = numbersPicked(1); 
        System.out.println("The winning numbers are: "); // Printing stuffs
        for (int i = 0; i < 5; i++){
            System.out.print(Lnum[i] + " ");
        }
        System.out.println();
        System.out.println("The numbers that you put in are: ");
        for (int i = 0; i < 5; i++){
            System.out.print(UserIn[i] + " ");
        }
        System.out.println();
        boolean match = userWins(UserIn, Lnum); 
        if (match == true){
            System.out.println("You win. Opps");
        } else {
            System.out.println("You lose. GG");
            System.out.println("You might want to check if the numbers that you input are valid. ");
        }
         
    }

    public static boolean userWins(int[] user, int[] winning){
        //returns true when user and winning are the same.
        //boolean userWins = false; 
        boolean userWins = Arrays.equals(user, winning);
        return userWins; 
    }

    public static int[] numbersPicked(int arraynum){
        //generates the random numbers for the lottery without duplication.
        int [] numbersPicked = new int [5]; 
        int counter = 0; // For comparing the next ran to the last rans if they are the same 
        for (int i = 0; i < 5; i++){
            numbersPicked [i] = (int)(Math.random() * 59 + 1); 
        }
        // Check for repetition 
        while (true){
            if (numbersPicked [0] == numbersPicked [1] || numbersPicked [0] == numbersPicked [2] || numbersPicked [0] == numbersPicked [3] || numbersPicked [0] == numbersPicked [4] || numbersPicked [1] == numbersPicked [2] ||numbersPicked [1] == numbersPicked [3] ||numbersPicked [1] == numbersPicked [4] ||numbersPicked [2] == numbersPicked [3] ||numbersPicked [2] == numbersPicked [4] || numbersPicked [3] == numbersPicked [4] ){
                for (int i = 0; i < 5; i++){
                    numbersPicked [i] = (int)(Math.random() * 59 + 1); 
                }
            } else{
                break; 
            }
        }
        return numbersPicked; 
    }
}  