// March 1
// CSE002 HW 05
// https://docs.google.com/document/d/1otzih0h_lOFH-FXWOjif2JjmC9vwiAf5Y07qruE3AXg/edit
// Into to loops HW
// ask for the course number, department name, the number of times it meets in a week, the time the class starts, the instructor name, and the number of students

// Import Scanner 
import java.util.Scanner;

public class Hw05   {
    public static void main(String[] args) {
        // Declear Scanner 
        Scanner NumInput = new Scanner(System.in); 

        // Course Number
        System.out.println("Enter only Course Number: ");
        // Must be an int
        int CourseNumber = 0; 
        // Check if the int is positve and an int
        while (true) {
            // if it is an int
            if (NumInput.hasNextInt()){
                CourseNumber = NumInput.nextInt(); 

                // if it is a positive 
                if (CourseNumber > 0){
                    break;   
                }   else {
                    // negative case 
                    System.out.println("Please !!! Enter only Course Number: ");
                }

            }   else {
                // not an int
                System.out.println("Please !!! Enter only Course Number: ");
                String junkWord = NumInput.next(); 
            }
        }

        // Department Name
        System.out.println("Enter only Department Name: ");    
        String DepartmentName = " "; 
        // Check for if it is a string
        while (true) {
            // if it is an string

            if (NumInput.hasNextDouble()){
                // Not the right type
                System.out.println("Wrong type. Please !!! Enter only Department Name: ");
                String junkWord = NumInput.next(); 

            }   else {
                // Right type
                DepartmentName = NumInput.next(); 
                 // Break out of the loop 
                 break;               
            }
        }


        // Number of meeting time 
        int MeetTime = 0; 
        System.out.println("Enter only the number of meeting times: "); 
        while (true) {
            // if it is an int
            if (NumInput.hasNextInt()){
                MeetTime = NumInput.nextInt(); 

                // if it is a positive 
                if (MeetTime > 0){
                    break;   
                }   else {
                    // negative case 
                    System.out.println("Please !!! Enter only the number of meeting times: ");
                }

            }   else {
                // not an int
                System.out.println("Please !!! Enter only the number of meeting times: ");
                String junkWord = NumInput.next(); 
            }
        }

        // Class Start time
        int StartTime = 0; 
        int StartHour = 0; 
        int StartMin = 0; 
        boolean ValidTime = true; 
        System.out.println("Enter only the class start time in military hours: "); 
        while (true) {
            // if it is an int
            if (NumInput.hasNextInt()){
                StartTime = NumInput.nextInt(); 
                // Find the hour
                StartHour = (int)StartTime / 100; 
                // Find the min
                StartMin = StartTime - StartHour * 100; 
                // If the time is in the right format
                ValidTime = StartTime / 100 >= 0    &&    StartTime / 100 <= 24; 

                // if it is a positive 
                if (  ValidTime && StartMin < 60 ){
                    break;   
                }   else {
                    // negative case 
                    System.out.println("Please !!! Enter only the class start time in military hours: ");
                    
                }

            }   else {
                // not an int
                System.out.println("Please !!! Enter only the class start time in military hours: ");
                String junkWord = NumInput.next(); 
            }
        }

        // Prof. Name 
        String ProName = ""; 
        // Check for if it is a string
        while (true) {
            // if it is an string

            if (NumInput.hasNextDouble()){
                // Not the right type
                System.out.println("Wrong type. Please !!! Enter only Department Name: ");
                String junkWord = NumInput.next(); 

            }   else {
                // Right type
                ProName = NumInput.next(); 
                 // Break out of the loop 
                 break;               
            }
        }


         // # of Student
         int StudentNumber = 0; 
         System.out.println("Enter only the number of students in the class: "); 
         while (true) {
             // if it is an int
             if (NumInput.hasNextInt()){
                StudentNumber = NumInput.nextInt(); 
 
                 // if it is a positive 
                 if (StudentNumber > 0 ){
                     break;   
                 }   else {
                     // negative case 
                     System.out.println("Please !!! Enter only the the number of students in the class: ");
                 }
 
             }   else {
                 // not an int
                 System.out.println("Please !!! Enter only the the number of students in the class: ");
                 String junkWord = NumInput.next(); 
             }
         } 
         
         
         // For better time format
         String AMPM = ""; 
         // If the class is AM or PM; 
         if (StartHour >= 0 && StartHour < 12) {
             AMPM = "AM"; 
         }  else {
             StartHour = StartHour - 12; 
             AMPM = "PM"; 
         }


         // Outputs 
         System.out.println("The course number is : " + CourseNumber);
         System.out.println("The department name is : " + DepartmentName);
         System.out.println("The class meet " + MeetTime + " times in a week");
         System.out.println("The class starts at: " + StartHour + ":" + StartMin + " " + AMPM);
         System.out.println("The name of the instructor is: " + ProName);
         System.out.println("The number of students in the class is: " + StudentNumber);

        







    }
}