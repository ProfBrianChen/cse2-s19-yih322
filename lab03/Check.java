// lab03
// CSE02
// Import Scanner
import java.util.Scanner;

public class Check {
// main method required for every Java program
    public static void main(String[] args) {
        // Declear Scanner
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Enter the original cost of the check in the form xx.xx: ");
        // Entering the cost of the check
        double checkCost = myScanner.nextDouble();
        
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
        // Entering the % of tips
        double tipPercent = myScanner.nextDouble();
        tipPercent /= 100; //We want to convert the percentage into a decimal value


        // Enter How many people in you party 
        System.out.print("Enter the number of people who went out to dinner:");
        // Number of people
        int numPeople = myScanner.nextInt();

        double totalCost; //Declear total cost
        double costPerPerson; // Declear totoal cost per person
        int dollars,   //whole dollar amount of cost 
            dimes, pennies; //for storing digits
                //to the right of the decimal point 
                //for the cost$ 
        totalCost = checkCost * (1 + tipPercent); // Total cost
        costPerPerson = totalCost / numPeople; //Per person price with tips
        //get the whole amount, dropping decimal fraction
        dollars=(int)costPerPerson;
        //get dimes amount, e.g., 
        // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
        //  where the % (mod) operator returns the remainder
        //  after the division:   583%100 -> 83, 27%5 -> 2 
        dimes=(int)(costPerPerson * 10) % 10;
        pennies=(int)(costPerPerson * 100) % 10;
        System.out.println("The total price including tips is : " + totalCost);
        System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

        


    }
}