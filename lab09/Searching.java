// CSE 022 Lab09
// https://docs.google.com/document/d/1oUzKIZG4AWvhPtPUARRoZQSjHkiN_eOJhXCfdxDg7Rw/edit 
import java.util.Arrays;
import java.util.Scanner;

public class Searching {
    public static void main(String[] args) {
        // Declare Scanner 
        Scanner input = new Scanner(System.in); 
        int SearchType = 0; 
        System.out.println("What type of Search would you like to prefrom? 1-Linear; 2-Binary");
        while (true){
            SearchType = input.nextInt(); 
            if(SearchType == 1 || SearchType == 2){
                break;
            } else {
                System.out.println("Invalid Input 1-Linear; 2-Binary");
            }
        }
        // Array Length 
        int ArraySize = 0; 
        System.out.println("Enter the array Length: ");
        while (true){
            ArraySize = input.nextInt(); 
            if(ArraySize > 0){
                break;
            } else {
                System.out.println("Enter the array Length with postive int");
            }
        }

        // Search for x 
        int SearchFor = 0; 
        System.out.println("Which int would you like to search for: ");
        while (true){
            SearchFor = input.nextInt(); 
            if(SearchFor > 0){
                break;
            } else {
                System.out.println("Enter the array Length with postive int");
            }
        }        
        // Array with random number that correspond to a random char with a random size 
        // Random size of the array
        if (SearchType == 1){
            int [] ranNum = new int [ArraySize]; 
            // Use the method to fill in the array 
            ranNum = ranNum(ArraySize);
            int LinearIndex = LinearSearch(ranNum, SearchFor); 
            System.out.println(Arrays.toString(ranNum));

            if (LinearIndex != -1){
                System.out.println("The int you were looking for is at: " + LinearIndex); 
            } else{
                System.out.println("The int you were looking NOT FOUND " + LinearIndex);  
            }

        } 
        // Binary array
        // Assending array 
        if (SearchType == 2){
            int [] ranNumAss = new int [ArraySize]; 
            ranNumAss = ranNumAss(ArraySize);
            int BinaryIndex = BinarySearch(ranNumAss, SearchFor); 
            System.out.println(Arrays.toString(ranNumAss));
            if (BinaryIndex != -1){
                System.out.println("The int you were looking for is at: " + BinaryIndex); 
            } else{
                System.out.println("The int you were looking NOT FOUND " + BinaryIndex);  
            }
        }
    }

    // Random Num gen, for creating the length of the array between 1-200 for the length of the arrray 
    public static int Random1_20 (int AValue){
        int Random10_20 = (int)(Math.random() * 20 + 1);
        return Random10_20;
    }

    // Fill in the array with random numbers from 1 to 20
    public static int [] ranNum (int arrayLength){
        // Declear the output array 
        int [] ranNum = new int [arrayLength]; 
        // Fill in the input array 
        for (int i = 0; i < arrayLength; i++){ 
            ranNum [i] = (int)(Math.random() * arrayLength + 1); 
        }
        return ranNum;
    } 
    
    // A method to generate an array of a given size that is filled with random ascending integers. RanNum are from 1 - 1000
    public static int [] ranNumAss (int arrayLength){
        // Declear the output array 
        int [] ranNum = new int [arrayLength]; 
        // Fill in the input array 
        for (int i = 0; i < arrayLength; i++){ 
           ranNum [i] = (int)(Math.random() * 10 + 1); 
        }
        Arrays.sort(ranNum);
        return ranNum;

    } 

    // Linear Search 
    public static int LinearSearch (int [] arr, int key){
        int size = arr.length;
        for(int i=0;i<size;i++){
            if(arr[i] == key){
                return i;
            }
        }
        return -1;
    }

    // Binary Search 
    public static int BinarySearch (int[] inputArr, int key){
        // int BinarySearchIndex = Arrays.binarySearch(arr, x);
        int start = 0;
        int end = inputArr.length - 1;
        while (start <= end) {
            int mid = (start + end) / 2;
            if (key == inputArr[mid]) {
                return mid;
            }
            if (key < inputArr[mid]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        return -1;
    }   
    
}