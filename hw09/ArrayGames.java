// HW 09 CSE 002 
// https://docs.google.com/document/d/1Fta9kWMcU95Kwc3yuHEXbtPh6JfSuQEIvz65cPtfSvc/edit
import java.util.Arrays;
import java.util.Scanner;

public class ArrayGames {
    public static void main(String[] args) {
        Scanner UserInput = new Scanner(System.in); 
        System.out.println("Enter 1 for insert, or 2 for shorten: ");
        int runType = 0; // Check for which one to run
        runType = CheckPosInt(runType); 
        switch (runType){
            case 1: // Insert
            int [] ArrA = RanIntArr10_20(1); 
            int [] ArrB = RanIntArr10_20(1); 
            int [] ArrIn = insert(ArrA, ArrB);
            System.out.print("Input1: ");
            PrintArray(ArrA);
            System.out.print("Input2: ");
            PrintArray(ArrB);
            System.out.print("Output: ");
            PrintArray(ArrIn);;
            break; 
            
            case 2: // Shorten
            int [] ArrS = RanIntArr10_20(1); 
            int x = (int)(Math.random()*8 + 1); // Steal at this index
            int [] theft = shorten(ArrS, x);
            System.out.print("Input1: ");
            PrintArray(ArrS);
            System.out.print("Input2: ");
            System.out.print(x);
            System.out.print("Output: ");
            PrintArray(theft);
            break; 

            default: 
            System.out.print("Error");

        }

    }
    // The method generate() will produce integer arrays of a random size between 10 and 20 members.
    public static int [] RanIntArr10_20 (int x){
        int arrLength = (int)(Math.random()*10 + 10); 
        int [] RanIntArr10_20 = new int[arrLength]; 
        for (int i = 0; i < arrLength; i++){
            RanIntArr10_20 [i] = i+1; 
        }
        return RanIntArr10_20; 
    }
    // The method print() will print out the members of an integer array input.
    public static void PrintArray (int [] arr){
        System.out.println(Arrays.toString(arr));
    }
    // The method insert() will accept two arrays as inputs. 
    public static int [] insert (int [] arrA, int [] arrB){
        int [] insert = new int [arrA.length + arrB.length]; 
        int RanIndex = (int)(Math.random()*10 + 1); // Insert at this spot 
        System.out.println("Insert at: " + RanIndex);
        for (int i = 0; i < RanIndex; i++){
            insert [i] = arrA[i];
        }
        for (int i = RanIndex; i < (RanIndex + arrB.length); i++){
            insert [i] = arrB[i - RanIndex]; 
        }
        for (int i = (RanIndex + arrB.length ); i < (arrA.length + arrB.length); i++){
            insert [i] = arrA[i - arrB.length]; 
        }
        return insert; 
    }

    // This method remove one index 
    public static int [] shorten (int [] arr, int x) {
        int [] shorten = new int [arr.length - 1]; 
        if (x > arr.length){
            return arr; 
        } else {
            for (int i = 0; i < x; i++){
                shorten [i] = arr [i];
            }
            for (int i = x; i < shorten.length; i++){
                shorten [i] = arr [i+1];
            }
            return shorten;
        }

    }


    // Check the input type if it is a positive integer 
    static int CheckPosInt (int inputByUser) {
        Scanner NumInput = new Scanner(System.in); 
        // Check for right type of input 
        while (true) {
           // if it is an int
           if (NumInput.hasNextInt()){
            inputByUser = NumInput.nextInt(); 
               // if it is a positive 
               if (inputByUser > 0){
                   break;   
               }   else {
                   // negative case 
                   System.out.println("Error: please type in an positive integer: ");
               }
           }   else {
               // not an int
               System.out.println("Error: please type in an positive integer: ");
               // Trash the bad input 
               String junkWord = NumInput.next(); 
           }
       }   
       return inputByUser; 
    }
}
